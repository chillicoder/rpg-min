class HomeController < ApplicationController
  def index
    @personajes = [['Paladín','Paladin'],'Elfo','Mago']
    @armas = ['Espada','Arco','Hechizo']

    @instance_personaje = nil
  end

  def attack
    @personajes = [['Paladín','Paladin'],'Elfo','Mago']
    @armas = ['Espada','Arco','Hechizo']

    arma = Object.const_get(params[:arma])
    personaje = Object.const_get(params[:personaje])

    @instance_personaje = personaje.new(arma.new)
    logger.debug @instance_personaje.atacar

    render :index 
  end

  def fixed_attack
    @personajes = [['Paladín','Paladin'],'Elfo','Mago']
    @armas = ['Espada','Arco','Hechizo']

    @personaje = Paladin.new(Espada.new)

    render :index
  end
end
