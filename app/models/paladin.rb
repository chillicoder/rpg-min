class Paladin
  attr_accessor :item

  def initialize(item)
    @item = item
  end

  def atacar
    "Conan ha #{@item.usar}"
  end
end
