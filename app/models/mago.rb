class Mago
  attr_accessor :item

  def initialize(item)
    @item = item
  end

  def atacar
    "Merlin ha #{item.usar}"
  end
end
